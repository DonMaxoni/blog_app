namespace :clean_uploads do
  desc 'Delete unused uploads'
  task delete_assets: :environment do
    Ckeditor::Asset.find_each do |asset|
      if Article.where('content LIKE ?', "%#{asset.data.content.url}%").limit(1).blank?
        asset.destroy
      end
    end
    puts 'Task is comleted!'
  end
end
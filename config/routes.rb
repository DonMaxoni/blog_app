Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  devise_for :bloggers, skip: [:sessions]
  as :blogger do
    get 'login', to: 'devise/sessions#new', as: :new_blogger_session
    post 'login', to: 'devise/sessions#create', as: :blogger_session
    get 'logout', to: 'devise/sessions#destroy', as: :destroy_blogger_session
  end

  root 'articles#index'
  resources :articles, only: [:show] do
    resources :comments, only: [:create]
  end

  get 'bio' => 'pages#bio'
  get 'about' => 'pages#about'
end

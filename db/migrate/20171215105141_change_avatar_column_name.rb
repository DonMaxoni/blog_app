class ChangeAvatarColumnName < ActiveRecord::Migration[5.1]
  def change
    change_table :comments do |t|
      t.rename :avatar, :avatar_path
    end
  end
end

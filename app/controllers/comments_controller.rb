class CommentsController < ApplicationController

  def create
    article = Article.find(params[:article_id])
    comment = article.comments.build(article_params)

    if comment.save
      flash[:success] = 'Comment is successfully created'
    else
      flash[:alert] = 'Something went wrong. PLease try again later'
    end

    redirect_to article_path(article)
  end

  private

  def article_params
    params.require(:comment).permit(:content)
  end
end

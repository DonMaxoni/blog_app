class ArticlesController < ApplicationController
  def index
    @articles = Article.order_by_recent.page(params[:page]).per(5)
  end

  def show
    @article = Article.find(params[:id])
  end
end

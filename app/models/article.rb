class Article < ApplicationRecord
  belongs_to :blogger
  has_many :comments, dependent: :destroy

  validates :title, presence: true, length: { maximum: 50 }
  validates :content, presence: true

  scope :order_by_recent, -> { order(created_at: :desc) }

  def self.random_article
    find(pluck(:id).sample)
  end

  # Label is not hidden, need to fix
  rails_admin do
    edit do
      configure :blogger_id do
        help ""
        label :hidden => true

        default_value do
          bindings[:view].current_blogger.id
        end

        view_helper do
          :hidden_field
        end
      end
    end
  end
end

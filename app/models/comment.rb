class Comment < ApplicationRecord
  before_create :random_avatar_path
  belongs_to :article

  validates :content, presence: true

  scope :order_by_recent, -> { order(created_at: :desc) }

  private

  def random_avatar_path
    self.avatar_path = "comments/avatar_#{rand(1..7)}.png"
  end
end

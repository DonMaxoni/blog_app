FactoryGirl.define do
  factory :blogger do
    name 'Max'
    email 'example@email.com'
    password 'password'
    password_confirmation 'password'
  end
end
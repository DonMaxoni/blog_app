FactoryGirl.define do
  factory :article do
    title 'Article Title'
    content 'Article Content'

    after(:build) do |article|
      article.blogger = article.blogger || Blogger.first || create(:blogger)
    end
  end
end
require 'rails_helper'

describe Comment do
  describe 'random avatar path selection' do
    let(:comment) { FactoryGirl.build(:comment) }

    it 'should select random avatar path before comment creation' do
      allow(comment).to receive(:rand) { 3 }
      comment.save

      expect(comment.reload.avatar_path).to eq('comments/avatar_3.png')
    end
  end
end
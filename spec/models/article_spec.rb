require 'rails_helper'

describe Article do
  describe 'random article selection' do
    let!(:articles) { FactoryGirl.create_list(:article, 3) }

    it 'should select random article id' do
      mock = Article.pluck(:id)
      allow(Article).to receive(:pluck) { mock }
      allow(mock).to receive(:sample) { Article.first.id }

      expect(Article.random_article).to eq(Article.first)
    end
  end
end
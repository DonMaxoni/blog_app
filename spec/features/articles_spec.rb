require 'rails_helper'

feature 'showing articles' do
  let!(:articles) { FactoryGirl.create_list(:article, 6) }

  scenario 'user can see articles list' do
    visit root_path
    expect(page).to have_content('Article Title', count: 5)
  end

  scenario 'user can paginate through articles list' do
    visit root_path
    within('nav.pagination') { click_link('Older Posts') }
    expect(page).to have_current_path(root_path(page: 2))
  end

  scenario 'user can read an article' do
    visit root_path
    within('.articles-list') { click_link('Article Title', match: :first) }
    expect(page).to have_content('Article Content')
  end
end

feature 'Guest can leave a comment to an article' do
  let(:article) { FactoryGirl.create(:article) }

  scenario 'can comment article' do
    visit article_path(article)
    fill_in 'comment_content', with: 'Awesome comment!'
    click_button 'Leave a comment'
    expect(page).to have_content('Comment is successfully created')
    expect(page).to have_content('Awesome comment!')
  end
end

feature 'random article' do
  context 'when articles are present' do
    before { FactoryGirl.create(:article) }

    scenario 'user can see link to random article' do
      visit root_path
      expect(page).to have_link 'Random Article'
    end
  end

  context 'when there are no articles' do
    scenario "user can't see link to random article" do
      expect(page).not_to have_link 'Random Article'
    end
  end
end

require 'rails_helper'

feature 'sign in' do
  let(:blogger) { FactoryGirl.create(:blogger) }

  scenario 'blogger can sign in' do
    visit new_blogger_session_path
    fill_in 'Email', with: blogger.email
    fill_in 'Password', with: 'password'
    click_button 'Log in'

    expect(page).to have_current_path(root_path)
  end
end
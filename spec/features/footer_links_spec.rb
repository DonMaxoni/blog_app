require 'rails_helper'

feature 'footer social media links' do
  before { visit root_path }

  scenario 'guest can see all social media links' do
    linkedin_href = 'https://www.linkedin.com/in/maxym-tesliuk-35a10717a/'

    within('footer') do
      expect(page).to have_link('LinkedIn', href: linkedin_href)
      expect(page).to have_link('Facebook',
                                href: 'https://www.facebook.com/maxym.tesliuk')
      expect(page).to have_link('Instagram',
                                href: 'https://www.instagram.com/ordinarymortal/')
      expect(page).to have_link('SoundCloud',
                                href: 'https://soundcloud.com/user-497787198')
    end
  end
end
